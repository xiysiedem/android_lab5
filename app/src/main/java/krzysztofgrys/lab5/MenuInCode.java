package krzysztofgrys.lab5;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MenuInCode extends AppCompatActivity{

    ContextMenu contextMenu;
    TextView txt1;
    TextView txt2;
    Boolean boldxD;
    Button back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_in_code);
        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        back = (Button) findViewById(R.id.button_back) ;

        registerForContextMenu(txt1);
        registerForContextMenu(txt2);

        txt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openContextMenu(txt1);
            }
        });

        txt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openContextMenu(txt2);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(Menu.NONE,0,1,"Powieksz czcionke");
        menu.add(Menu.NONE,1,2,"Pomniejsz czcionke");
        menu.add(Menu.NONE,2,3,"Tekst Pogrubiony");
        menu.add(Menu.NONE,3,4,"Tekst pochylony");
        SubMenu sm = menu.addSubMenu(Menu.NONE,4,5,"Zmien czcionke");
        sm.add(Menu.NONE,5,1,"Czcionka1");
        sm.add(Menu.NONE,6,2,"Czcionka2");
        sm.add(Menu.NONE,7,3,"Czcionka3");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.checkablemenu, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                if(txt1.getTextSize()>100){
                    Toast.makeText(this, "osiagnieto maksymalny rozmiar tekstu!", Toast.LENGTH_SHORT)
                            .show();
                }else{
                    Log.v("asd","Asd "+ txt1.getTextSize());
                    txt1.setTextSize((float) (txt1.getTextSize()+txt1.getTextSize()*0.5));
                    txt2.setTextSize((float) (txt2.getTextSize()+txt2.getTextSize()*0.5));
                }
                break;
            case 1:
                if(txt1.getTextSize()<10){
                    Toast.makeText(this, "osiagnieto minimalny rozmiar tekstu!", Toast.LENGTH_SHORT)
                            .show();
                }else{
                    // Log.v("asd","Asd "+ txt1.getTextSize());
                    txt1.setTextSize((float) (txt1.getTextSize()-txt1.getTextSize()*0.7));
                    txt2.setTextSize((float) (txt2.getTextSize()-txt2.getTextSize()*0.7));
                }
                break;
            case 2:
                // TODO naprawic to xDD
                if(txt1.getTypeface()!=null){
                    if(txt1.getTypeface().getStyle()==Typeface.BOLD){
                        txt1.setTypeface(null , Typeface.NORMAL);
                        txt2.setTypeface(null , Typeface.NORMAL);
                        boldxD=false;
                    }else{
                        txt1.setTypeface(null, Typeface.BOLD);
                        txt2.setTypeface(null , Typeface.BOLD);
                        boldxD=true;
                    }
                }else{
                    txt1.setTypeface(null, Typeface.BOLD);
                    txt2.setTypeface(null , Typeface.BOLD);
                    boldxD=true;

                }
                break;
            case 3:
                if(txt1.getTypeface()!=null) {
                    if (txt1.getTypeface().getStyle() == Typeface.BOLD || txt1.getTypeface().getStyle() == Typeface.BOLD_ITALIC ) {
                        if (txt1.getTypeface().getStyle() == Typeface.ITALIC ||txt1.getTypeface().getStyle() == Typeface.BOLD_ITALIC  ) {
                            txt1.setTypeface(null, Typeface.BOLD);
                            txt2.setTypeface(null, Typeface.BOLD);
                        } else {
                            txt1.setTypeface(null, Typeface.BOLD_ITALIC);
                            txt2.setTypeface(null, Typeface.BOLD_ITALIC);
                        }
                    }else{
                        if(txt1.getTypeface().getStyle() == Typeface.ITALIC){
                            txt1.setTypeface(null, Typeface.NORMAL);
                            txt2.setTypeface(null, Typeface.NORMAL);
                        }else{
                            txt1.setTypeface(null, Typeface.ITALIC);
                            txt2.setTypeface(null, Typeface.ITALIC);
                        }

                    }
                }else {
                    if(boldxD!=null){
                        txt1.setTypeface(null, Typeface.BOLD_ITALIC);
                        txt2.setTypeface(null, Typeface.BOLD_ITALIC);
                    }else{
                        txt1.setTypeface(null, Typeface.ITALIC);
                        txt2.setTypeface(null, Typeface.ITALIC);
                    }

                }

                break;
            case 5:
                Typeface typeface = Typeface.createFromAsset(getAssets(),"GoodDog.otf");
                txt2.setTypeface(typeface);

                break;
            case 6:
                typeface = Typeface.createFromAsset(getAssets(),"Pacifico.ttf");
                txt2.setTypeface(typeface);
                break;
            case 7:
                typeface = Typeface.createFromAsset(getAssets(),"Roboto-Black.ttf");
                txt2.setTypeface(typeface);
                break;
            case R.id.blue_check:
                ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
                viewGroup.setBackgroundColor(Color.BLUE);
                item.setChecked(true);
                break;
            case R.id.red_check:
                viewGroup = (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
                viewGroup.setBackgroundColor(Color.RED);
                item.setChecked(true);
                break;
            case R.id.white_check:
                viewGroup = (ViewGroup) ((ViewGroup) this.findViewById(android.R.id.content)).getChildAt(0);
                viewGroup.setBackgroundColor(Color.WHITE);
                item.setChecked(true);
                break;
            default:
                break;
        }

        return true;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if(v.getId() == R.id.txt1) {
            menu.setHeaderTitle("Wybierz kolor");
            menu.add(Menu.NONE,0,1,"Czarny");
            menu.add(Menu.NONE,1,2,"Czerwony");
            menu.add(Menu.NONE,2,3,"Niebieski");


        }else{
            menu.setHeaderTitle("Wybierz Zmiane");
            menu.add(Menu.NONE,4,1,"Odwórć");
            menu.add(Menu.NONE,5,2,"ZAMIEN NA DUZE LITERY");
            menu.add(Menu.NONE,6,3,"zamien na male litery");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case 0:
                txt1.setTextColor(Color.BLACK);
                break;
            case 2:
                txt1.setTextColor(Color.BLUE);
                break;
            case 1:
                txt1.setTextColor(Color.RED);
                break;
            case 4:
                String reverse =  new StringBuilder(txt2.getText()).reverse().toString();
                txt2.setText(reverse);
                break;
            case 6:
                txt2.setText(txt2.getText().toString().toLowerCase());
                break;
            case 5:
                txt2.setText(txt2.getText().toString().toUpperCase());
                break;
            default:
                return super.onContextItemSelected(item);
        }
        return true;
    }

}
